#!/usr/bin/ruby

# usage rangify <filename.midi>

csv = `midicsv #{ARGV[0]}`

new_csv = csv.lines.map do |line|
  line.chomp!
  fields = line.split(/,\s*/)
  if fields[2] =~ /Note_o/
    # range 52-100
    pitch = fields[4].to_i
    while pitch < 38
      pitch += 12
    end
    while pitch > 76
      pitch -= 12
    end
    fields[4] = pitch.to_s
    fields.join ", "
  else
    line
  end
end.uniq.join "\n"

name, _ = ARGV[0].split "."
new_csv_name = "#{name}_g.csv"
new_midi_name = "#{name}_g.midi"
File.open(new_csv_name, "w") do |sink|
  sink.write(new_csv)
end
`csvmidi #{new_csv_name} #{new_midi_name}`